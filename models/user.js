const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'firstName is required']
	},
	lastName: {
		type: String,
		required: [true, 'lastName is required']
	},
	email: {
		type: String,
		required: [true, 'email is required']
	},
	mobileNo: {
		type: String,
		required: [true, 'mobileNo is required']
	},
	password: {
		type: String,
		required: [true, 'password is required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	}, 
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, 'Course ID is required']
			}, //this will identify the course/subject
			enrolledOn: {
				type: Date,
				default: new Date() //the default will come from the current date and time of the machine
			}, //this describes the time and date the student enrolled for the course
			status: {
				type: String,
				default: 'Enrolled' //alternative values can be Completed or Cancelled
			}//this describes if the student is accepted or rejected in the class
		}
	] //this will be an array of objects, a student can enroll on more than 1 subject
});

module.exports = mongoose.model('User', userSchema);