const User = require('../models/user');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Course = require('../models/course');
//create a function that will register a new user

module.exports.emailExists = (params) => {
	return User.find({email: params.email}).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10) //this is called hasing and encrypts the password value
	})

	return user.save().then((user, userErr) => {
		return (userErr) ? false : true
	}) //will return the success or failure of the saving function
}

module.exports.login = (params) => {
	//check if the user exists in the database
	return User.findOne({email: params.email}).then(user => {
		if(user === null) {return false}
		const passwordMatch = bcrypt.compareSync(params.password, user.password)
		if(passwordMatch) {
			return { access: auth.createAccessToken(user.toObject())}
		} else {
			return false;
		}
		
	})
}

module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}

module.exports.enroll = (params) => {
	return User.findById(params.userId).then((user) => {
		user.enrollments.push({ courseId: params.courseId})

		return user.save().then((user, err) => {
			return Course.findById(params.courseId).then((course) => {
				course.enrollees.push({ userId: params.userId})

				return course.save().then((course, err) => {
					return (err) ? false : true 
				})
			})
		})
	})
}