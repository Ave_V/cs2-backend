const Course = require('../models/course');

//create a function that will register a new user

const capitalize = (string) => {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

module.exports.courseExists = (params) => {
	return Course.find({name: capitalize(params.name)}).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.addCourse = (params) => {
	let course = new Course({
		name: capitalize(params.name),
		description: params.description,
		price: params.price,
	})

	return course.save().then((course, err) => {
		return (err) ? false : true
	}) //will return the success or failure of the saving function
}

//create a new function that will display all of the courses that has an active status of "true"
module.exports.getAll = () => {
	return Course.find({isActive: true}).then(courses => courses)
}

`x`
module.exports.get = (params) => {
	return Course.findById(params.courseId).then(course => course)
}

module.exports.archive = (params) => {
	const updates = {isActive: false}
	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}