//the purpose of this module is to hold all the script regarding the app's authorization

const jwt = require('jsonwebtoken');
const secret = 'pogchamp';
//declare a secret word/phrase that will be used as signature/access token for the project. this is to verify the request coming from other apps

//3 parts of data as JWT:
//HEADER, SIGNATURE, PAYLOAD

//create a function to authorize a user using the access token
module.exports.createAccessToken = (user) => {
	//identify the props/keys of the user to be verified
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin //to identify the role of the user and their restrictions
	};
	return jwt.sign(data, secret, {});
	//throught the use of sign() method, a synchronous signature is made with a default HMAC(hash-based message authentication code)
}

//2 access tokens is for another layer of security and to verify if the access token came from the authorized/proper origin

//the real value of the access token is hashed when generated
//create a function to verify whether the access token is correct
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization //authorization is a form of request "header" commonly used for HTTP basic authorization. it will set if the server requested authorization and the browser then prompted the user for a username/password

	//create a control structure to describe/identify whether the token will be allowed to pass

	if(typeof token !== "undefined") {
		//if token has a value from the authorization property
		token = token.slice(7, token.length) //the token's data type is STRING
		//1st param - identify the number of chara to be sliced off of the string value
		//2nd param - 
		//when the JWT is generated it will include 7 additional characters for addl security
		return jwt.verify(token, secret, (err, data) => {
			return (err) ? res.send({auth: 'failed'}) : next()
			//next() is used for the middleware, by invoking it will let the middleware proceed to the next function
		})
	} else {
		return res.send({auth: 'failed'});
	}
}

module.exports.decode = (token) => {
	//create a control structure to determine the response if an access token is captured
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			return (err) ? null : jwt.decode(token, { complete: true }).payload //payload is for options
		})
	} else {
		return null
	}
}