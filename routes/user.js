const express = require('express');
const router = express.Router();
const auth = require('../auth')
const UserController = require('../controllers/user');

// [SECTION] Primary Routes
router.post('/email-exists', (req, res) => {
	UserController.emailExists(req.body).then(result => res.send(result));
});

//register a new user
router.post('/register', (req, res) => {
	UserController.register(req.body).then(result => res.send(result));
})

//this route will be for user login 
router.post('/login', (req,res) => {
	UserController.login(req.body).then(result => res.send(result))
})

router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(user => res.send(user))
})

// [SECTION] Secondary Routes (STRETCH GOALS)

router.post('/enroll', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	
	UserController.enroll(params).then((result) => res.send(result))
}) 
//the user should be logged in and correct access rights before enrolling


module.exports = router;

