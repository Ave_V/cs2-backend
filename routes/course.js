const express = require('express');
const router = express.Router();
const auth = require('../auth');
const CourseController = require('../controllers/course');

router.post('/addCourse', (req, res) => {
	CourseController.addCourse(req.body).then(result => res.send(result));
});


//this will get the course properties of the course ID requested
router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId
	CourseController.get({courseId}).then(course => res.send(course))
})

router.post('/', auth.verify, (req, res) => {
	CourseController.add(req.body).then(result => res.send(result))
}) 
//this will get all the courses available for enrollment (has isActive property as true)
router.get('/', (req, res) => {
	CourseController.getAll().then(courses => res.send(courses))
})

router.post('/course-exists', (req, res) => {
	CourseController.courseExists(req.body).then(result => res.send(result));
});

router.delete('/:id', auth.verify, (req,res) => {
	const courseId = req.params.id
	CourseController.archive({courseId}.then(result => res.send(result)))
})

module.exports = router;
