const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// initializes express dependency on entry point
const app = express()

const corsOptions = {
	origin: ['https://serene-island-34692.herokuapp.com/', 'https://git.heroku.com/serene-island-34692.git', 'https://ave_v.gitlab.io/cs2-frontend'],
    optionsSuccessStatus: 200
}

app.use(cors()); //allows for sharing between 2 origins

// employs the use of dotenv for sensitive server information
require('dotenv').config()
const port = process.env.PORT;
const connectionString = process.env.DB_CONNECTION_STRING;

//these scripts make my requests readable. DO NOT FORGET TO ADD IT
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//removes deprecations from console
mongoose.connect('mongodb+srv://Ave_V:uNhAtgFaCUVxhZ3c@bootcampcluster.iufpk.mongodb.net/booking_system_db?retryWrites=true&w=majority' || connectionString,
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course');

app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);

// connects to mongoDB
let db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', () => console.log("Now connected to cloud database: MongoDB Atlas"))


// displays in port webpage connection status
app.get('/', function(req, res){
	res.send(`Server up on port ${port}`);
})

// notifies console of connection status
app.listen(port, () => console.log(`API is now Online in port ${ process.env.PORT || 4000}`))